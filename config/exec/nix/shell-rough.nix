with import <nixpkgs> {};

let

  ##
  ## Python
  ##
  pip-python-packages = python-packages: with python-packages; [
    aws
    boto3
    botocore
  ];
  python-with-packages = python2.withPackages pip-python-packages;

  ##
  ## Ansible
  ##
  genericAnsible = { version, sha256, py ? python2 }: py.pkgs.buildPythonPackage rec {
    pname = "ansible";
    inherit version;

    outputs = [ "out" "man" ];

    src = fetchurl {
      url = "https://releases.ansible.com/ansible/${pname}-${version}.tar.gz";
      inherit sha256;
    };

    prePatch = ''
      sed -i "s,/usr/,$out," lib/ansible/constants.py
    '';

    postInstall = ''
      for m in docs/man/man1/*; do
        install -vD $m -t $man/share/man/man1
      done
    '';

    doCheck = false;
    dontStrip = true;
    dontPatchELF = true;
    dontPatchShebangs = false;

    propagatedBuildInputs = with py.pkgs; [
      pycrypto paramiko jinja2 pyyaml httplib2 boto six netaddr dnspython jmespath
    ];

    meta = with stdenv.lib; {
      homepage = http://www.ansible.com;
      description = "A simple automation tool";
      license = with licenses; [ gpl3 ] ;
      maintainers = with maintainers; [ jgeerds joamaki ];
      platforms = with platforms; linux ++ darwin;
    };
  };

  ansible_2_1 = genericAnsible {
    version = "2.1.6.0";
    sha256 = "e5d7c88412b9dbf5a5efec774648c1d1018491f1ac3e506f0004262ab3aa1a3a";
  };


  ansible_2_4 = genericAnsible {
    version = "2.4.4.0";
    sha256  = "0n1k6h0h6av74nw8vq98fmh6q4pq6brpwmx45282vh3bkdmpa0ib";
  };

  ansible_2_5 = genericAnsible {
    version = "2.5.11";
    sha256  = "07rhgkl3a2ba59rqh9pyz1p661gc389shlwa2sw1m6wwifg4lm24";
  };

  ansible_2_6 = genericAnsible {
    version = "2.6.7";
    sha256  = "10pakw9k9wd3cy1qk3ah2253ph7c7h3qzpal4k0s5lschzgy2fh0";
  };

  ansible_2_7 = genericAnsible {
    version = "2.7.4";
    sha256 = "498bb0581bb8ff00d77f1643f20f63b69227c13d4464419d10450cc3bc37365c";
  };

  ##
  ## Kops
  ##
  createKops = { version , sha256 }: buildGoPackage rec {
    name = "kops-${version}";
    inherit version;

    goPackagePath = "k8s.io/kops";

    src = fetchFromGitHub {
      rev = version;
      owner = "kubernetes";
      repo = "kops";
      # This needss to be the sha256 hash of `https://github.com/kubernetes/kops/archive/1.10.0.tar.gz`
      # The hash needs to be of the UNPACKED directory. Obtain the hash using this command:
      # `nix-prefetch-url https://github.com/kubernetes/kops/archive/1.9.0.tar.gz --type sha256 --unpack`
      inherit sha256; 
    };

    buildInputs = [go-bindata];
    subPackages = ["cmd/kops"];


    buildFlagsArray = ''
      -ldflags=
          -X k8s.io/kops.Version=${version}
          -X k8s.io/kops.GitVersion=${version}
    '';

    preBuild = ''
      (cd go/src/k8s.io/kops
      go-bindata -o upup/models/bindata.go -pkg models -prefix upup/models/ upup/models/...)
    '';

    postInstall = ''
      mkdir -p $bin/share/bash-completion/completions
      mkdir -p $bin/share/zsh/site-functions
      $bin/bin/kops completion bash > $bin/share/bash-completion/completions/kops
      $bin/bin/kops completion zsh > $bin/share/zsh/site-functions/_kops
    '';

    meta = with stdenv.lib; {
      description = "Easiest way to get a production Kubernetes up and running";
      homepage = https://github.com/kubernetes/kops;
      license = licenses.asl20;
      maintainers = with maintainers; [offline zimbatm];
      platforms = platforms.unix;
    };
  };

  kops_1_4 = createKops {
    version = "v1.4.4";
    sha256 = "0mmbz8m924k7ykdbmmv65zlk3r1w82i1cdf1qw6yc6g5zhxgg319";
  };

  kops_1_10 = createKops {
    version = "1.10.0";
    sha256 = "1ga83sbhvhcazran6xfwgv95sg8ygg2w59vql0yjicj8r2q01vqp";
  };

  kops_1_11_beta = createKops {
    version = "1.11.0-beta.1";
    sha256 = "01xh1rlny72cdfaj85n122lrm0a9l7khk01hncnd444w7l9vxkfy";
  };

  kops_1_11 = createKops {
    version = "1.11.0";
    sha256 = "0kfy";
  };

  ##
  ## kubectl
  ## 

  createKubectl = { version , sha256 }: stdenv.mkDerivation rec {
    name = "kubernetes-${version}";
    inherit version; 

    src = fetchFromGitHub {
      owner = "kubernetes";
      repo = "kubernetes";
      rev = "v${version}";
      inherit sha256; 
    };

    buildInputs = [ removeReferencesTo makeWrapper which go_1_11 rsync go-bindata ];

    outputs = ["out" "man" "pause"];
    components = [
      "cmd/kubeadm"
      "cmd/kubectl"
      "cmd/kubelet"
      "cmd/kube-apiserver"
      "cmd/kube-controller-manager"
      "cmd/kube-proxy"
      "cmd/kube-scheduler"
      "test/e2e/e2e.test"
    ];

    postPatch = ''
      substituteInPlace "hack/lib/golang.sh" --replace "_cgo" ""
      substituteInPlace "hack/generate-docs.sh" --replace "make" "make SHELL=${stdenv.shell}"
      # hack/update-munge-docs.sh only performs some tests on the documentation.
      # They broke building k8s; disabled for now.
      echo "true" > "hack/update-munge-docs.sh"

      patchShebangs ./hack
    '';

    # WHAT="${concatStringsSep " " components}";

    postBuild = ''
      ./hack/generate-docs.sh
      (cd build/pause && cc pause.c -o pause)
    '';

    installPhase = ''
      mkdir -p "$out/bin" "$out/share/bash-completion/completions" "$out/share/zsh/site-functions" "$man/share/man" "$pause/bin"

      cp _output/local/go/bin/* "$out/bin/"
      cp build/pause/pause "$pause/bin/pause"
      cp -R docs/man/man1 "$man/share/man"

      cp cluster/addons/addon-manager/namespace.yaml $out/share
      cp cluster/addons/addon-manager/kube-addons.sh $out/bin/kube-addons
      patchShebangs $out/bin/kube-addons
      substituteInPlace $out/bin/kube-addons \
        --replace /opt/namespace.yaml $out/share/namespace.yaml
      wrapProgram $out/bin/kube-addons --set "KUBECTL_BIN" "$out/bin/kubectl"

      $out/bin/kubectl completion bash > $out/share/bash-completion/completions/kubectl
      $out/bin/kubectl completion zsh > $out/share/zsh/site-functions/_kubectl
    '';

    preFixup = ''
      find $out/bin $pause/bin -type f -exec remove-references-to -t ${go_1_10} '{}' +
    '';

    meta = {
      description = "Production-Grade Container Scheduling and Management";
      homepage = https://kubernetes.io;
    };
  };

  kubectl_1_6 = createKubectl {
    version = "1.6.7";
    sha256 = "1svvcy7kld5jb7hpimfiwg3kmnmy8xax8mzhxhzn3b7cdglv3b80";
  };

  kubectl_1_11 = createKubectl {
    version = "1.11.5";
    sha256 = "0ifncv46l62sx213fxkn9xkh87hysshigp829jz9ncv9k3j5jcnj";
  };

  kubectl_1_12 = createKubectl {
    version = "1.12.3";
    sha256 = "0y227qzv7hsibf0sil5ylfdvkfsd43qlsyprc1dwgbj8igjl6q2d";
  };

  kubectl_1_13 = createKubectl {
    version = "1.13.0";
    sha256 = "0smj053v7j6pbrm9pd3sh4a4bckckkrgcxbry2ndznavsqnly3m9";
  };


  ## 
  ## Helm
  ##

  helm_1_6 = createHelm {
    version = "1.6";
    sha256 = "";
  };

  helm_1_13 = createHelm {
    version = "1.13";
    sha256 = "";
  };

  ##
  ## Golang
  ##

in stdenv.mkDerivation rec {
  name = "env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    ansible_2_7
    aws
    docker
    go_1_11
    #kops
    kops_1_11_beta
    kubectl_1_13
    kubernetes-helm
    lua5_3
    python-with-packages
    terraform
    vim
  ];

  ## Environment Variables
  TEST_ENVVAR = "BlahBlahBlah";
}

# invoke with `nix-shell ./default.nix --pure`

# refactor: break each build input into its own .nix file and import it here