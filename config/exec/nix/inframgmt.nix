with import <nixpkgs> {};

let
  pip-python-packages = python-packages: with python-packages; [
    aws
    boto3
    botocore
  ];
  python-with-packages = python2.withPackages pip-python-packages;



in stdenv.mkDerivation rec {
  name = "env"; 
  env = buildEnv { name = name; paths = buildInputs; };



  buildInputs = [
    ansible_2_7
    #aws
    docker
    go_1_11
    kops
    kubectl
    lua
    terraform
    vim
    packer
    _1password
    python-with-packages
  ];



  # Environment variables
  # find a way to pass in the keys so that we can use
  # AWS_ACCESS_KEY = "";
  # AWS_SECRET_ACCESS_KEY="";
}