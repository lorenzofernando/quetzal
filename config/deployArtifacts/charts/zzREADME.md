# Helm Charts

3 groups of charts:

1. Utility
Utility charts are to be installed on all clusters. 

2. Management
Management charts are to be installed on clusters fulfilling the 'Manager' cluster duty. 

3. Application



Q: Should I keep the templated configuraiton files in this subdirectory, or in `/config/ansible`? I'm leaning towards HERE so that we don't split the logic for charts between two very different directories. Keep similar things together. 