Consider creating unique names for roles, e.g. kops-deadbeef, kops-beefdead, kops-abcdefg. This will allow us to version our role usage and have multiple instances exist at the same time. 



Proposal:
Use the `kops` role to generate other kops roles. 
* pick between a randomly generated string and a list of random items. e.g.:
  - kops-ower8uada
  - kops-kdeqroueq
  - kops-vzcverwqi

  // VS
  - kops-apple
  - kops-banana
  - kops-cumin
  - kops-dogwood