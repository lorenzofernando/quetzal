export NODE_SIZE=${NODE_SIZE:-m5.large}
export MASTER_SIZE=${MASTER_SIZE:-m5.large}
export ZONES=${ZONES:-"us-east-2a,us-east-2b,us-east-2c"}
export KOPS_STATE_STORE="s3://quetzallazteuq-kops-configuration/"
kops create cluster rhodia.k8s.heylorenzo.com \
--node-count 5 \
--zones $ZONES \
--node-size $NODE_SIZE \
--master-size $MASTER_SIZE \
--master-zones $ZONES \
--networking calico \
--topology private \
--bastion="true" \
--kubernetes-version="v1.11.6" \
--network-cidr="10.192.0.0/16" \
--encrypt-etcd-storage \
--cloud=aws \
--master-volume-size=256 \
--node-volume-size=256 \
--topology="private" \
--ssh-access="67.84.103.48/32" \
--state=$KOPS_STATE_STORE \
--dry-run -oyaml > manifest.yaml


# OIDC issues in kops 1.10 noted here: https://github.com/kubernetes/kops/issues/6107

# Changes to generated file:
# - kubeAPIServer block
# - min/max node instance group size (cannot be set via flag)
# masterInternalName: api.internal.rhodia.k8s.beepbeep.com
# googleClientID: blahblahblahblahblahblahblahblahblahblah.apps.googleusercontent.com

# Suggestions:
#  * validate cluster: kops validate cluster
#  * list nodes: kubectl get nodes --show-labels
#  * ssh to the bastion: ssh -A -i ~/.ssh/id_rsa admin@bastion.rhodia.k8s.beepbeep.com
#  * the admin user is specific to Debian. If not using Debian please use the appropriate user based on your OS.
#  * read about installing addons at: https://github.com/kubernetes/kops/blob/master/docs/addons.md.