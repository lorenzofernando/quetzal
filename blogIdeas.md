# Blog Ideas

* KMS Key Rotation
* Adding SSH Keys to servers using user-init
* Creating a tool to vet the images running on a cluster.
* Commit signing, docker image signing, helm chart signing
* only running trusted containers on a cluster
* Creating a Nix provisioner for Packer