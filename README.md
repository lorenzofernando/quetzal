# Quetzal

Behold my technical plumage, an example of cloud infrastructure engineering. 

[Trello Board](https://trello.com/b/BPNBKyrz/quetzal)

## Tools
* Ansible 2.7
* Kubernetes 1.13
* AWS CLI

## Game Plan

1. Ansible
  * use for everything.
  * wrap executions in a Nix shell.
2. Kubernetes
  * Let's Encrypt
  * Prometheus
  * RBAC
  * Helm charts for (un)common applications
    - SonarQube
    - Elasticsearch -- ELK completely
    - Prometheus tools
    - Jenkins
    - Phabricator
  * Create Docker Images from scrat
  * Reach Items:
    - Deploy and use Istio & write a post on utility of service meshes
    - Write an operator from scratch
    - Write a kubernetes custom resource and deploy it
    - kafka chart
    - Cassandra chart
    - yubikey for cluster authentication
    - automated analysis of:
      * Helm charts
      * docker containers
      * clusters
    - cloud-native deployment via spinnaker or Jenkins X or similar
    - optionally launch a katacontainer cluster
    - chaosmonkey
3. CI/CD
  * for both application deployment and infrastructure management
4. Monitoring, Logging, Metrics
5. AWS, aside from EC2
  * Cloudformation templates for ALL THE THINGS
    - RDS
    - EC2
    - S3
    - EBS
  
  * Design for security
6. General Purpose programming
  - code kata
  - Hacker rank
  - web app
  - kubernetes custom resource
  - kubernetes operator
  - repo with schoolwork
  - aws cli commands


### Features
* completely infrastructure-as-code
* single point of entry
* demonstrate scalability
* explain design choices
* reusability

## Technologies mentioned in SRE/DevOps/Infra engineer Job Postings

search terms (indeed)
SRE kubernetes

ansible

Improve and keep documentation up to date.

* Engage in capacity planning, demand forecasting, software performance analysis, and systems tuning.
* Measure and monitor availability, latency and overall system health.
* solid knowledge of web application architecture

## Helm Chart Bundles (aka Cluster Duties aka Cluster Roles):

### Utility
* Utility charts should be installed on every kubernetes cluster


### Management
Management charts create tools used in the mangement of infrastucture. Includines monitoring & logging, CI/CD, security & vuln scanning

### Application

